var express = require('express');
var router = express.Router();
var rp = require('request-promise').defaults({simple: false})
var url = require('url')

router.get('/api/users', (req, res) => {

  let since = req.query.since
  let uri = `https://api.github.com/users?since=${since}`

  let requestOptions = {
    uri,
    resolveWithFullResponse: true,
    headers: {
      "User-Agent": "Shawand-Partners-API"
    },
    json: true
  }

  rp(requestOptions).then((response) => {

    let body = response.body

    let link = response.headers['link']

    if(link == undefined){
      console.log(body)
      res.send(body)
    }
    else{

      link = link.split(";")[0].replace('<', '').replace(">", '')
  
      let search = url.parse(link, true).search
  
      let pathname = url.parse(req.url).pathname
      link = `${pathname}${search}`
      
      res.set('link', link)
      res.status(200).send(body)
    }

  })
  

})

router.get('/api/users/:username/details', (req, res) => {

  let username = req.params.username
  let uri = `https://api.github.com/users/${username}`

  let requestOptions = {
    uri,
    resolveWithFullResponse: true,
    headers: {
      "User-Agent": "Shawand-Partners-API"
    },
    json: true
  }

  rp(requestOptions).then((response) => {

    let body = response.body
    
    res.status(200).send(body)

  })

})

router.get('/api/users/:username/repos', (req, res) => {

  let username = req.params.username
  let uri = `https://api.github.com/users/${username}/repos`

  let requestOptions = {
    uri,
    resolveWithFullResponse: true,
    headers: {
      "User-Agent": "Shawand-Partners-API"
    },
    json: true
  }

  rp(requestOptions).then((response) => {

    let body = response.body

    res.status(200).send(body)

  })

})

module.exports = router;
