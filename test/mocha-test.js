
let chai = require('chai')
let server = require('../bin/www')
let chaiHttp = require('chai-http')

let should = chai.should();
chai.use(chaiHttp)

const USERNAME = 'defunkt'

describe('Users', function(){

    this.timeout(10000);

    it('Get User List', (done) => {

        chai.request(server)
        .get('/api/users?since=0')
        .end(function(err, res){

            if(err) throw err
            
            res.should.have.status(200)
            res.should.have.header('link');
            
            done()
        })
        
    })

    it('Get User Details', (done) => {

        chai.request(server)
        .get('/api/users/'+USERNAME+'/details')
        .end(function(err, res){

            if(err) throw err
            
            res.should.have.status(200)

            res.body.should.have.property('id')
            res.body.should.have.property('login')
            res.body.should.have.property('url')
            res.body.should.have.property('created_at')
            
            done()
        })
        
    })

    it('Get User Repos', (done) => {

        chai.request(server)
        .get('/api/users/'+USERNAME+'/repos')
        .end(function(err, res){

            if(err) throw err
            
            res.should.have.status(200)

            res.body.should.not.empty
            res.body.should.be.a('array')
            
            done()
        })
        
    }) 

})





